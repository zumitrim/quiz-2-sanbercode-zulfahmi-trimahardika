SELECT customer.name as customer_name, SUM(orders.amount) as total_amount 
FROM customer 
inner join orders 
ON customer.id = orders.customer_id 
GROUP BY customer.name order by customer.id